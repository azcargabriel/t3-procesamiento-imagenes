#define _DEBUG

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "opencv2/ml/ml.hpp"
#include <opencv2/core/core.hpp>

#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace cv;
using namespace cv::ml;

int uniforms[58] = {0, 1, 2, 3, 4, 6, 7, 8, 12, 14, 15, 16, 24, 28, 30, 31, 32, 
	48, 56, 60, 62, 63, 64, 96, 112, 120, 124, 126, 127, 128, 129,
	131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225,
	227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254, 255};


//HISTOGRAM ---------------------------------------------------------------------------------------

class HistogramLBP {
	private:
		float values[59];
		int quantity;

	public:
		HistogramLBP();
		HistogramLBP(Mat input);
		void add(int n);
		void normalize();
		float* getValues();
		void printH();
};

HistogramLBP::HistogramLBP() {
	quantity = 0;
	for(int i = 0; i < 59; i++) {
		values[i] = 0;
	}
}

HistogramLBP::HistogramLBP(Mat input) {
	quantity = 0;
	for(int i = 0; i < 59; i++) {
		values[i] = 0;
	}

	for (int r = 0; r < input.rows; r++) {
		for (int c = 0; c < input.cols; c++) {
			this->add(int(input.at<float>(r, c)));
		}
	}
}

void HistogramLBP::add(int n) {
	values[n]++;
	quantity++;
}

void HistogramLBP::normalize() {
	for(int i = 0; i < 59; i++) {
		values[i] /= float(quantity);
	}
}

float* HistogramLBP::getValues(){
	return values;
}

void HistogramLBP::printH() {
	for(int i = 0; i < 59; i++) {
		cout << values[i] << " ";
	}
	cout << "Q: " << quantity << endl;
}

//END HISTOGRAM -----------------------------------------------------------------------------------

//MANEJO DE DATOS ---------------------------------------------------------------------------------

int getLBPValue(int n) {
	for(int i = 0; i < 58; i++) {
		if(n == uniforms[i]) {
			return i + 1;
		}
	}

	return 0;
}

Mat uniformLBP(Mat input) {
	Mat output = Mat::zeros(input.rows - 2, input.cols - 2, CV_32FC1);

	for (int r = 1; r < input.rows - 1; r++) {
		for (int c = 1; c < input.cols - 1; c++) {
			uchar value = input.at<uchar>(r, c);
			int result = 0;
			
			result += (value > input.at<uchar>(r - 1, c - 1)) ? 0 : 1;
			result += (value > input.at<uchar>(r - 1, c)) ? 0 : pow(2, 1);
			result += (value > input.at<uchar>(r - 1, c + 1)) ? 0 : pow(2, 2);
			result += (value > input.at<uchar>(r, c + 1)) ? 0 : pow(2, 3);
			result += (value > input.at<uchar>(r + 1, c + 1)) ? 0 : pow(2, 4);
			result += (value > input.at<uchar>(r + 1, c)) ? 0 : pow(2, 5);
			result += (value > input.at<uchar>(r + 1, c - 1)) ? 0 : pow(2, 6);
			result += (value > input.at<uchar>(r, c - 1)) ? 0 : pow(2, 7);
			
			output.at<float>(r - 1, c - 1) = getLBPValue(result);
		}
	}

	return output;
}

void get2x2HLBP(Mat input, float f_vector[236]) {
	int w_w = input.cols;
	int w_h = input.rows;

	Mat firstQ = input(Rect(Point(0, 0), Point(w_w/2, w_h/2)));
	Mat secondQ = input(Rect(Point(w_w/2, 0), Point(w_w, w_h/2)));
	Mat thirdQ = input(Rect(Point(0, w_h/2), Point(w_w/2, w_h)));
	Mat fourthQ = input(Rect(Point(w_w/2, w_h/2), Point(w_w, w_h)));

	HistogramLBP hfiQ(uniformLBP(firstQ));
	HistogramLBP hseQ(uniformLBP(secondQ));
	HistogramLBP hthQ(uniformLBP(thirdQ));
	HistogramLBP hfoQ(uniformLBP(fourthQ));

	hfiQ.normalize();
	hseQ.normalize();
	hthQ.normalize();
	hfoQ.normalize();

	for(int i = 0; i < 59*4; i++) {
		if(i / 59 == 0) f_vector[i] = hfiQ.getValues()[i];
		else if(i / 59 == 1) f_vector[i] = hseQ.getValues()[i - 59];
		else if(i / 59 == 2) f_vector[i] = hthQ.getValues()[i - 2*59];
		else f_vector[i] = hfoQ.getValues()[i - 3*59];
	}
}

void generateDB(std::string dbName[5], int max, std::string prefix) {
	ofstream f, f2, f3;
	std::stringstream ss1, ss2, ss3;
	ss1 << prefix << "test.csv";
	ss2 << prefix << "training.csv";
  	f.open(ss1.str());
  	f2.open(ss2.str());
  	
  	for(int z = 0; z < max; z++) {
  		std::string folder = dbName[z];

  		std::vector<int> indexes;
  		for(int i = 0; i < 200; i++) {
  			indexes.push_back(i);
  		}
  		std::random_shuffle(indexes.begin(), indexes.end());

		for(int i = 0; i < 200; i++) {
			std::stringstream ss;
			ss << folder << indexes[i] << ".jpg";
			std::string fileName = ss.str();
			Mat img = imread(fileName);

			Mat img_gray;
			cvtColor(img, img_gray, CV_BGR2GRAY);

			float mergeHistograms[59*4];
			get2x2HLBP(img_gray, mergeHistograms);

			if(i < 60) { //Test 60 muestras
				for(int k = 0; k < 59*4; k++) {
					f << mergeHistograms[k] << ",";
				}
				f << z << endl;
			}
			else { //Training (140 muestras)
				for(int i = 0; i < 59*4; i++) {
					f2 << mergeHistograms[i] << ",";
				}
				f2 << z << endl;
			}
		}
	}

	f.close();
	f2.close();
}

void generateAllDB() {
	cout << "Generating..." << endl;
	std::string s[5] = {"../images/Asian/","../images/Black/","../images/Indian/","../images/Others/","../images/White/"};
	std::string ss[5] = {"../images/Asian/","../images/Black/", "", "", ""};
	generateDB(s, 5, "all_");
	generateDB(ss, 2, "ab_");
}

//END DE DATOS ---------------------------------------------------------------------------------

//CLASIFICACION --------------------------------------------------------------------------------

float accuracy(Mat output, Mat labels) {
	float counter = 0.f;
	for(int i = 0; i < output.rows; i++) {
		if(output.at<float>(i, 0) == labels.at<float>(i, 0)) {
			counter++;
		}
	}
	return counter / output.rows;
}

void svmAvB() {
	Ptr<TrainData> raw_data = TrainData::loadFromCSV("ab_training.csv", 0, -1, -1);
	Mat trainingData = raw_data->getTrainSamples();
	float tData[280][236];
	int tLabels[280];

	for(int i = 0; i < 280; i++) {
		for(int k = 0; k < 235; k++) {
			tData[i][k] = trainingData.at<float>(i, k);
		}
		tLabels[i] = int(i/140);
	}

	Mat trainingDataMat(280, 236, CV_32FC1, tData);
	Mat trainingLabelsMat(280, 1, CV_32SC1, tLabels);

	Ptr<SVM> svm = SVM::create();
	svm->setType(SVM::C_SVC);
	svm->setKernel(SVM::LINEAR);
	svm->setTermCriteria(TermCriteria(CV_TERMCRIT_ITER, 1000, 1e-6));
	svm->train(trainingDataMat, 0, trainingLabelsMat);

	Ptr<TrainData> test_data = TrainData::loadFromCSV("ab_test.csv", 0, -1, -1);
	Mat testData = test_data->getTrainSamples();
	float teData[120][236];
	float teLabels[120];

	for(int i = 0; i < 120; i++) {
		for(int k = 0; k < 235; k++) {
			teData[i][k] = testData.at<float>(i, k);
		}
		teLabels[i] = int(i/60);
	}

	Mat testDataMat(120, 236, CV_32FC1, teData);
	Mat testLabelsMat(120, 1, CV_32SC1, teLabels);

	Mat testResults = Mat(120, 1, CV_32SC1);
	svm->predict(testDataMat, testResults);

	cout << accuracy(testResults, testLabelsMat) << endl;
}

void svmAll(){
	Ptr<TrainData> raw_data = TrainData::loadFromCSV("all_training.csv", 0, -1, -1);
	Mat trainingData = raw_data->getTrainSamples();
	float tData[700][236];
	int tLabels[700];

	for(int i = 0; i < 700; i++) {
		for(int k = 0; k < 235; k++) {
			tData[i][k] = trainingData.at<float>(i, k);
		}
		tLabels[i] = int(i/140);
	}

	Mat trainingDataMat(700, 236, CV_32FC1, tData);
	Mat trainingLabelsMat(700, 1, CV_32SC1, tLabels);

	Ptr<SVM> svm = SVM::create();
	svm->setType(SVM::C_SVC);
	svm->setKernel(SVM::LINEAR);
	svm->setTermCriteria(TermCriteria(CV_TERMCRIT_ITER, 1000, 1e-6));
	svm->train(trainingDataMat, 0, trainingLabelsMat);

	Ptr<TrainData> test_data = TrainData::loadFromCSV("all_test.csv", 0, -1, -1);
	Mat testData = test_data->getTrainSamples();
	float teData[300][236];
	float teLabels[300];

	for(int i = 0; i < 300; i++) {
		for(int k = 0; k < 235; k++) {
			teData[i][k] = testData.at<float>(i, k);
		}
		teLabels[i] = int(i/60);
	}

	Mat testDataMat(300, 236, CV_32FC1, teData);
	Mat testLabelsMat(300, 1, CV_32SC1, teLabels);

	Mat testResults = Mat(300, 1, CV_32SC1);
	svm->predict(testDataMat, testResults);

	cout << accuracy(testResults, testLabelsMat) << endl;
}

void rfAvB() {
	Ptr<TrainData> raw_data = TrainData::loadFromCSV("ab_training.csv", 0, -1, -1);
	Mat trainingData = raw_data->getTrainSamples();
	float tData[280][236];
	int tLabels[280];

	for(int i = 0; i < 280; i++) {
		for(int k = 0; k < 235; k++) {
			tData[i][k] = trainingData.at<float>(i, k);
		}
		tLabels[i] = int(i/140);
	}

	Mat trainingDataMat(280, 236, CV_32FC1, tData);
	Mat trainingLabelsMat(280, 1, CV_32SC1, tLabels);

	Ptr<RTrees> randomForest = RTrees::create();
	randomForest->train(trainingDataMat, 0, trainingLabelsMat);

	Ptr<TrainData> test_data = TrainData::loadFromCSV("ab_test.csv", 0, -1, -1);
	Mat testData = test_data->getTrainSamples();
	float teData[120][236];
	float teLabels[120];

	for(int i = 0; i < 120; i++) {
		for(int k = 0; k < 235; k++) {
			teData[i][k] = testData.at<float>(i, k);
		}
		teLabels[i] = int(i/60);
	}

	Mat testDataMat(120, 236, CV_32FC1, teData);
	Mat testLabelsMat(120, 1, CV_32SC1, teLabels);

	Mat testResults = Mat(120, 1, CV_32SC1);
	randomForest->predict(testDataMat, testResults);

	cout << accuracy(testResults, testLabelsMat) << endl;
}

void rfAll() {
	Ptr<TrainData> raw_data = TrainData::loadFromCSV("all_training.csv", 0, -1, -1);
	Mat trainingData = raw_data->getTrainSamples();
	float tData[700][236];
	int tLabels[700];

	for(int i = 0; i < 700; i++) {
		for(int k = 0; k < 235; k++) {
			tData[i][k] = trainingData.at<float>(i, k);
		}
		tLabels[i] = int(i/140);
	}

	Mat trainingDataMat(700, 236, CV_32FC1, tData);
	Mat trainingLabelsMat(700, 1, CV_32SC1, tLabels);

	Ptr<RTrees> randomForest = RTrees::create();
	randomForest->train(trainingDataMat, 0, trainingLabelsMat);

	Ptr<TrainData> test_data = TrainData::loadFromCSV("all_test.csv", 0, -1, -1);
	Mat testData = test_data->getTrainSamples();
	float teData[300][236];
	float teLabels[300];

	for(int i = 0; i < 300; i++) {
		for(int k = 0; k < 235; k++) {
			teData[i][k] = testData.at<float>(i, k);
		}
		teLabels[i] = int(i/60);
	}

	Mat testDataMat(300, 236, CV_32FC1, teData);
	Mat testLabelsMat(300, 1, CV_32SC1, teLabels);

	Mat testResults = Mat(300, 1, CV_32SC1);
	randomForest->predict(testDataMat, testResults);

	cout << accuracy(testResults, testLabelsMat) << endl;
}

//END CLASIFICACION ----------------------------------------------------------------------------

int main(int argc, void** argv) {
	if(1 < argc)
		generateAllDB();
	svmAvB();
	svmAll();
	rfAvB();
	rfAll();
	return 0;
}